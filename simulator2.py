import pandas as pd
import random

_TYPES = ["bond first", "stock first", "recession aware", "lowest interest", "highest interest", "stock only",
          "roth first", "taxed first"]


def d4d6():
    return sum([random.randint(1, 10) for _ in range(10)]) - 48


def monthly_stock():
    return 1 + d4d6() / 12


def d8d2():
    return sum([random.randint(0, 8) for _ in range(0, 2)]) / 2


def monthly_bond():
    return 1 + d8d2() / 12

class Account:
    name = "Test"
    roth = True
    deposit_percentage = 1
    Type = "IRA"
    bond_age = 1000
    bond_stock = 0
    stocks = 0
    bonds = 0


def simulator(portfolio: list,
              age=25,
              death=100,
              income=50000,
              retirement_withdrawal=1,
              retirement_age=65,
              tax_bracket=0.2,
              recession_probability=0,
              random_returns=True,
              rebalance=False,
              must_pass=False,
              withdrawal_strategy: _TYPES = "bond first"):
    results = []
    price_index = 1
    income = income
    year = age
    while year < death and sum([x.stocks + x.bonds for x in portfolio]) >= 0:
        tax = round(income * tax_bracket, 2)
        tax_paid = 0
        stock_return = (1 + d4d6() / 100)
        bond_return = (1 + d8d2() / 100)
        if year < retirement_age:
            for account in portfolio:
                if year > account.bond_age:
                    account.stocks += income * account.bond_stock * account.deposit_percentage
                    account.bonds += income * account.bond_stock * account.deposit_percentage
                else:
                    account.stocks += income * account.deposit_percentage
                if account.roth:
                    tax_paid = income * account.deposit_percentage * tax_bracket
        if year >= retirement_age:
            withdrawal = income * retirement_withdrawal
            remaining = withdrawal
            counter = 0
            while remaining > 0:
                if withdrawal_strategy == "bond first":  # withdraw from bond account if the bond account has money
                    if portfolio[counter].bonds > withdrawal:
                        remaining = 0
                        portfolio[counter].bonds -= withdrawal
                        if not portfolio[counter].roth:
                            portfolio[counter].bonds -= tax
                            tax_paid += tax
                    elif portfolio[counter].stocks > income:
                        remaining = 0
                        portfolio[counter].stocks -= income
                        if not portfolio[counter].roth:
                            portfolio[counter].stocks -= tax
                            tax_paid = tax
                elif withdrawal_strategy == "stock first":  # withdraw from stock account if stock account has money
                    if portfolio[counter].stocks > income and portfolio[counter].stocks > income:
                        remaining = 0
                        portfolio[counter].stocks -= income
                        if not portfolio[counter].roth or portfolio[counter].roth == 'no tax':
                            portfolio[counter].stocks -= tax
                            tax_paid = tax
                    elif portfolio[counter].bonds > income:
                        remaining = 0
                        portfolio[counter].bonds -= income
                        if not portfolio[counter].roth:
                            portfolio[counter].bonds -= tax
                            tax_paid = tax
                elif withdrawal_strategy == "recession aware":  # withdraw from stocks unless if there is a recession,
                    if stock_return < 0 and portfolio[counter].bonds > income:
                        remaining = 0
                        portfolio[counter].bonds -= income
                        if not portfolio[counter].roth:
                            portfolio[counter].bonds -= tax
                            tax_paid = tax
                    elif portfolio[counter].stocks > income:
                        remaining = 0
                        portfolio[counter].stocks -= income
                        if not portfolio[counter].roth or portfolio[counter].roth == 'no tax':
                            portfolio[counter].stocks -= tax
                            tax_paid = tax
                elif withdrawal_strategy == "lowest interest":  # withdraw from the fund with the lowest interest rate
                    if stock_return > bond_return > income:
                        remaining = 0
                        portfolio[counter].bonds -= income
                        if not portfolio[counter].roth:
                            portfolio[counter].bonds -= tax
                            tax_paid = tax
                    elif portfolio[counter].stocks > income:
                        remaining = 0
                        portfolio[counter].stocks -= income
                        if not portfolio[counter].roth or portfolio[counter].roth == 'no tax':
                            portfolio[counter].stocks -= tax
                            tax_paid = tax
                elif withdrawal_strategy == "highest interest":  # withdraw from the fund with the lowest interest rate
                    if stock_return < bond_return and portfolio[counter].bonds > income:
                        remaining = 0
                        portfolio[counter].bonds -= income
                        if not portfolio[counter].roth:
                            portfolio[counter].bonds -= tax
                            tax_paid = tax
                    elif portfolio[counter].stocks > income:
                        remaining = 0
                        portfolio[counter].stocks -= income
                        if not portfolio[counter].roth or portfolio[counter].roth == 'no tax':
                            portfolio[counter].stocks -= tax
                            tax_paid = tax
                elif withdrawal_strategy == "stock only":
                    if portfolio[counter].stocks > income:
                        remaining = 0
                        portfolio[counter].stocks -= income
                        if not portfolio[counter].roth or portfolio[counter].roth == 'no tax':
                            portfolio[counter].stocks -= tax
                            tax_paid = tax
                counter += 1
        for account in portfolio:
            if random_returns:
                account.stocks = round(account.stocks * stock_return, 2)
                account.bonds = round(account.bonds * bond_return, 2)
            else:
                account.stocks = round(account.stocks * 1.08, 2)
                account.bonds = round(account.bonds * 1.04, 2)
                if random.randint(0, 100) / 100 < recession_probability:
                    account.stocks *= 0.8
            if rebalance:
                if account.bond_stock:
                    if age >= account.bond_age:
                        # move money from stock account to bond account to rebalance portfolio if option selected
                        portfolio_value = account.stocks + account.bonds
                        if account.bonds / portfolio_value < account.bond_stock:
                            target_bond = portfolio_value * account.bond_stock
                            transfer = round(target_bond - account.bonds, 2)
                            account.stocks -= transfer
                            account.bonds -= transfer
        income *= 1.02
        price_index *= .98
        results.append({'stocks': sum(account.stocks for account in portfolio),
                        'bonds': sum(account.bonds for account in portfolio),
                        'price_index': price_index,
                        'income': round(income / price_index, 2),
                        'age': year,
                        'tax': round(tax_paid, 2),
                        'stock return': stock_return,
                        'bond return': bond_return})
        year += 1
        if must_pass and sum(account.bonds + account.stocks for account in portfolio) < 0:
            return False
    return pd.DataFrame(results)
