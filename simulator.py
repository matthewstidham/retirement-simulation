import pandas as pd
import random

_TYPES = ["bond first", "stock first", "recession aware", "lowest interest", "highest interest", "stock only"]


def d4d6():
    return sum([random.randint(1, 10) for _ in range(10)]) - 48


def monthly_stock():
    return 1 + d4d6() / 12


def d8d2():
    return sum([random.randint(0, 8) for _ in range(0, 2)]) / 2


def monthly_bond():
    return 1 + d8d2() / 12


def simulator(age=25,
              death=100,
              income=50000,
              retirement_withdrawal=1,
              retirement_age=65,
              deposit_percentage=0.1,
              bond_stock=0,
              bond_age=1000,
              roth=False,
              tax_bracket=0.2,
              recession_probability=0,
              random_returns=True,
              rebalance=False,
              withdrawal_strategy: _TYPES = "bond first",
              must_pass=False):
    results = []
    stock_balance = 0
    bond_balance = 0
    price_index = 1
    income = income
    withdrawal = 0
    year = age
    while year < death and stock_balance + bond_balance >= 0:
        stock_return = (1 + d4d6() / 100)
        bond_return = (1 + d8d2() / 100)
        if year < retirement_age:
            if year > bond_age:
                stock_balance += income * (1 - bond_stock) * deposit_percentage
                bond_balance += income * bond_stock * deposit_percentage
            else:
                stock_balance += income * deposit_percentage
            if roth:
                tax_paid = income * tax_bracket
            else:
                tax_paid = (income * (1 - deposit_percentage)) * tax_bracket
        if year >= retirement_age:
            tax_paid = 0
            if income:
                withdrawal = income * retirement_withdrawal
                income = 0
            if withdrawal_strategy == "bond first":  # withdraw from bond account if the bond account has money
                if bond_balance > withdrawal:
                    bond_balance -= withdrawal
                    if not roth:
                        bond_balance -= withdrawal * tax_bracket
                        tax_paid = withdrawal * tax_bracket
                else:
                    stock_balance -= withdrawal
                    if not roth or roth == 'no tax':
                        stock_balance -= withdrawal * tax_bracket
                        tax_paid = withdrawal * tax_bracket
            elif withdrawal_strategy == "stock first":  # withdraw from stock account if stock account has money
                if stock_balance > withdrawal and stock_balance > withdrawal:
                    stock_balance -= withdrawal
                    if not roth or roth == 'no tax':
                        stock_balance -= withdrawal * tax_bracket
                        tax_paid = withdrawal * tax_bracket
                else:
                    bond_balance -= withdrawal
                    if not roth:
                        bond_balance -= withdrawal * tax_bracket
                        tax_paid = withdrawal * tax_bracket
            elif withdrawal_strategy == "recession aware":  # withdraw from stocks unless if there is a recession,
                if stock_return < 0 and bond_balance > withdrawal:
                    bond_balance -= withdrawal
                    if not roth:
                        bond_balance -= withdrawal * tax_bracket
                        tax_paid = withdrawal * tax_bracket
                else:
                    stock_balance -= withdrawal
                    if not roth or roth == 'no tax':
                        stock_balance -= withdrawal * tax_bracket
                        tax_paid = withdrawal * tax_bracket
            elif withdrawal_strategy == "lowest interest":  # withdraw from the fund with the lowest interest rate
                if stock_return > bond_return > withdrawal:
                    bond_balance -= withdrawal
                    if not roth:
                        bond_balance -= withdrawal * tax_bracket
                        tax_paid = withdrawal * tax_bracket
                else:
                    stock_balance -= withdrawal
                    if not roth or roth == 'no tax':
                        stock_balance -= withdrawal * tax_bracket
                        tax_paid = withdrawal * tax_bracket
            elif withdrawal_strategy == "highest interest":  # withdraw from the fund with the lowest interest rate
                if stock_return < bond_return and bond_balance > withdrawal:
                    bond_balance -= withdrawal
                    if not roth:
                        bond_balance -= withdrawal * tax_bracket
                        tax_paid = withdrawal * tax_bracket
                else:
                    stock_balance -= withdrawal
                    if not roth or roth == 'no tax':
                        stock_balance -= withdrawal * tax_bracket
                        tax_paid = withdrawal * tax_bracket
            elif withdrawal_strategy == "stock only":
                stock_balance -= withdrawal
                if not roth or roth == 'no tax':
                    stock_balance -= withdrawal * tax_bracket
                    tax_paid = withdrawal * tax_bracket
        if random_returns:
            stock_balance = round(stock_balance * stock_return, 2)
            bond_balance = round(bond_balance * bond_return, 2)
        else:
            stock_balance = round(stock_balance * 1.08, 2)
            bond_balance = round(bond_balance * 1.04, 2)
            if random.randint(0, 100) / 100 < recession_probability:
                stock_balance *= 0.8
        if rebalance:
            if bond_stock:
                if age >= bond_age:
                    # move money from stock account to bond account to rebalance portfolio if option selected
                    portfolio_value = stock_balance + bond_balance
                    if bond_balance / portfolio_value < bond_stock:
                        target_bond = portfolio_value * bond_stock
                        transfer = round(target_bond - bond_balance, 2)
                        stock_balance -= transfer
                        bond_balance -= transfer
        income *= 1.02
        withdrawal *= 1.02
        price_index *= .98
        results.append({'stock_balance': stock_balance,
                        'bond_balance': bond_balance,
                        'price_index': price_index,
                        'income': round(income / price_index, 2),
                        'age': year,
                        'bond_stock': bond_stock,
                        'roth': roth,
                        'tax': round(tax_paid, 2),
                        'withdrawal': withdrawal,
                        'deposit': income * deposit_percentage,
                        'stock return': stock_return,
                        'bond return': bond_return})
        year += 1
        if must_pass and stock_balance + bond_balance < 0:
            return False
    return pd.DataFrame(results)
